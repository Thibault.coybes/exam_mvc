-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 24 fév. 2023 à 10:27
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `mvc_exam`
--

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `sujet` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `sujet`, `email`, `message`, `created_at`) VALUES
(1, 'Bonjour', 'tib_cbs@outlook.fr', 'Bonjour c\'est thibault', '2023-02-24 09:31:08'),
(2, 'Bonjour', 'tib_cbees226@outloo.fr', 'zedzdzedzedzed', '2023-02-24 09:35:46'),
(3, 'Eval Surprise', 'quidelantoine@gmail.com', 'Eval finis.', '2023-02-24 10:00:22'),
(4, 'Bonjour', 'tib_cbees226@outloo.fr', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '0000-00-00 00:00:00'),
(5, 'Bonjour', 'tib_cbaessceks22d6@outloo.fr', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '0000-00-00 00:00:00'),
(6, 'Bonjour', 'tib_cbees226@outloo.fr', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2010-11-10 00:00:00'),
(7, 'Bonjour', 'aaaaa@gmail.com', 'aaaaaaaaaaaaaaaaaaaaaaaaaa', '0000-00-00 00:00:00'),
(8, 'aaaaaaaaaaaaaaaaaaaaa', 'aaaa@gmail.com', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '0000-00-00 00:00:00'),
(9, 'Bonjour', 'aaaa@gmail.com', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaa', '0000-00-00 00:00:00'),
(10, 'Bonjour', 'tib_cbaees226@outloo.fr', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '0000-00-00 00:00:00'),
(11, 'Bonjour', 'tib_cbees226@outloo.fr', 'aaaaaaaaaaaaaaaaaaaaaaaaa', '0000-00-00 00:00:00');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
