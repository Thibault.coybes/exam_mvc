<?php

namespace App\Controller;

use App\model\ContactModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class ContactController extends AbstractController
{
    public function contactForm()
    {
        $error = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation;
            $error['sujet'] = $v->textValid($post['sujet'], 'sujet', 3, 100);
            $error['message'] = $v->textValid($post['message'], 'message', 3, 100);
            $error['email'] = $v->emailValid($post['email']);
            if ($v->IsValid($error)) {
                ContactModel::insert($post);
                $this->redirect('listingContact');
            }
        }
        $form = new Form($error);
        $this->render('app.contact.contactForm', array(
            'form' => $form,
        ), 'base');
    }

    public function listingContact()
    {
        $allContact = ContactModel::all();
        $this->render('app.contact.listingContact', array(
            'allContacts' => $allContact,
        ), 'base');
    }
}
