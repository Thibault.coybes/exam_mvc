<?php

namespace App\model;

use Core\App;
use Core\Kernel\AbstractModel;

class ContactModel extends AbstractModel
{
    protected static $table = 'contact';
    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (sujet, message, email, created_at) VALUES (?,?,?,FORMAT(NOW(), 'H:i:s'))",
            array($post['sujet'], $post['message'], $post['email'])
        );
    }
}
