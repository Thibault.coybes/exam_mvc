<div class="wrap">
    <form action="" method="post">
        <?= $form->label('sujet') ?>
        <?= $form->input('sujet') ?>
        <?= $form->error('sujet') ?>

        <?= $form->label('email') ?>
        <?= $form->input('email') ?>
        <?= $form->error('email') ?>

        <?= $form->label('message') ?>
        <?= $form->textarea('message') ?>
        <?= $form->error('message') ?>

        <?= $form->submit('submitted') ?>
    </form>
</div>